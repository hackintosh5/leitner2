@file:Suppress("SpellCheckingInspection")

package dev.hack5.leitner.patterns

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PatternBuilderTest {
    @Test
    fun testPatternMatchesSimple() = testPatternMatches("hello world. goodbye world!", "hello world. goodbye world!")
    @Test
    fun testPatternMatchesPunctuation() = testPatternMatches("hello world. goodbye world!", "hello world goodbye world")
    @Test
    fun testPatternMatchesPunctuationComplex() = testPatternMatches("hello   world's# goodbye.!!\"£$%^&*()@:[]{} world", "hello world's. goodbye world!")
    @Test
    fun testPatternMatchesOptional() {
        testPatternMatches("hello world goodbye", "hello world goodbye[ world]")
        testPatternMatches("hello world goodbye world", "hello world goodbye[ world]")
    }
    @Test
    fun testPatternMatchesMutuallyExclusive() {
        testPatternMatches("hello world hello world", "hello world{ hello / goodbye }world")
        testPatternMatches("hello world goodbye world", "hello world{ hello / goodbye }world")
    }

    @Test
    fun testPatternNotMatchesSimple() = testPatternNotMatches("hello worldgoodbye world!", "hello world. goodbye world!")
    @Test
    fun testPatternNotMatchesExtra() = testPatternNotMatches("hello world. goodbye world!", "hello world. goodbye!")
    @Test
    fun testPatternNotMatchesOptional() {
        testPatternNotMatches("hello goodbye", "hello world goodbye[ world]")
        testPatternNotMatches("hello world world ", "hello world goodbye[ world]")
    }
    @Test
    fun testPatternNotMatchesMutuallyExclusive() {
        testPatternNotMatches("hello world hellish world", "hello world{ hello / goodbye }world")
        testPatternNotMatches("hello world goodbye cruel world", "hello world{ hello / goodbye }world")
    }

    private fun testPatternMatches(actual: String, pattern: String) {
        assertTrue(Pattern(pattern).matches(actual))
    }

    private fun testPatternNotMatches(actual: String, pattern: String) {
        assertFalse(Pattern(pattern).matches(actual))
    }
}