package dev.hack5.leitner.calendar

import dev.hack5.leitner.models.LeitnerTerm
import dev.hack5.leitner.models.QuestionAnswerTerm
import kotlinx.datetime.*
import kotlin.test.Test
import kotlin.test.assertEquals

private val BASE_DATETIME = LocalDateTime(year = 2020, month = Month.SEPTEMBER, dayOfMonth = 17, hour = 0, minute = 0, second = 0, nanosecond = 0)
private val BASE_DATE = BASE_DATETIME.date
private val BASE_INSTANT = BASE_DATETIME.toInstant(TimeZone.UTC)
private val TEST_FREQUENCIES = listOf(
        1 to 2,
        3 to 4,
        5 to 8,
        12 to 16,
        28 to 31,
)

class StudyCalendarTest {

    @Test
    fun testCalendarOneTermOneDay() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to terms,
                )
        )
    }

    @Test
    fun testCalendarTwoTermsOneDay() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(0).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0]),
                        days(2) to listOf(terms[1]),
                )
        )
    }

    @Test
    fun testCalendarTwoTermsTwoDays() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(1).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0]),
                        days(2) to listOf(terms[1]),
                )
        )
    }

    @Test
    fun testCalendarTwoTermsTwoDaysSplit() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(2).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0]),
                        days(3) to listOf(terms[1]),
                )
        )
    }

    @Test
    fun testCalendarThreeTermsOneDay() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a2"), days(0).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0], terms[2]),
                        days(2) to listOf(terms[1]),
                )
        )
    }

    @Test
    fun testCalendarThreeTermsTwoDays() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a2"), days(1).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0]),
                        days(2) to listOf(terms[1]),
                        days(3) to listOf(terms[2]),
                )
        )
    }

    @Test
    fun testCalendarThreeTermsThreeDays() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(1).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a2"), days(1).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0]),
                        days(2) to listOf(terms[1]),
                        days(3) to listOf(terms[2]),
                )
        )
    }

    @Test
    fun testCalendarThreeTermsMixedLevelsOneDay() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 1),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(0).time, 1),
                LeitnerTerm(QuestionAnswerTerm("q", "a2"), days(0).time, 0),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[2]),
                        days(3) to listOf(terms[0]),
                        days(4) to listOf(terms[1]),
                )
        )
    }

    @Test
    fun testCalendarThreeTermsMixedLevelsTrickyDays() {
        val terms = listOf(
                LeitnerTerm(QuestionAnswerTerm("q", "a0"), days(0).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a1"), days(1).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a2"), days(1).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a3"), days(2).time, 0),
                LeitnerTerm(QuestionAnswerTerm("q", "a4"), days(-1).time, 2),
                LeitnerTerm(QuestionAnswerTerm("q", "a5"), days(-10).time, 3),
        )
        return testCalendar(
                terms,
                mapOf(
                        days(1) to listOf(terms[0]),
                        days(2) to listOf(terms[1]),
                        days(3) to listOf(terms[2]),
                        days(4) to listOf(terms[3]),
                        days(5) to listOf(terms[4]),
                        days(6) to listOf(terms[5]),
                )
        )
    }

    fun testCalendar(terms: List<LeitnerTerm>, calendar: Map<LocalDate, List<LeitnerTerm>>) {
        assertEquals(calendar, StudyCalendar(terms, TEST_FREQUENCIES).organize(BASE_DATE))
    }

    fun days(days: Int) = BASE_DATE + DatePeriod(days = days)
}

private val LocalDate.time get() = atTime(0, 0)