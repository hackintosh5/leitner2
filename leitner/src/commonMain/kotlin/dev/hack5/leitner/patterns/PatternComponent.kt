package dev.hack5.leitner.patterns

abstract class PatternComponent {
    var active = false

    open fun descend(component: PatternComponent) {
        throw NotImplementedError()
    }

    open fun ascend() {
        throw NotImplementedError()
    }

    open fun append(char: Char) {
        throw NotImplementedError()
    }

    open fun match(text: String, sibling: Int = 0, soFar: Int = 0) = 0

    open fun empty() = false

    open val activeComponent
        get() = if (active) this else null
}

abstract class BasePatternComponent: PatternComponent() {
    val components = mutableListOf<PatternComponent>()
    abstract fun isValidComponent(component: PatternComponent): Boolean

    override fun descend(component: PatternComponent) {
        if (active) {
            if (!isValidComponent(component)) throw IllegalArgumentException("$component is not a valid child for $this")
            components.add(component)
            component.active = true
            active = false
        } else {
            components.last().descend(component)
        }
    }

    override fun ascend() {
        if (components.last().active) {
            if (components.last().empty()) {
                components.removeLast().active = false
            } else {
                components.last().active = false
            }
            active = true
        } else {
            components.last().ascend()
        }
    }

    override fun append(char: Char) {
        if (active) {
            throw NotImplementedError()
        } else {
            components.last().append(char)
        }
    }

    override fun match(text: String, sibling: Int, soFar: Int): Int {
        var ret = 0
        components.forEachIndexed { i, component ->
            val thisRet = component.match(text, i, soFar + ret)
            if (thisRet == -1)
                return -1
            ret += thisRet
        }
        return ret
    }

    override fun empty() = components.isEmpty()

    override val activeComponent
        get() = if (active) this else components.last().activeComponent

    override fun toString(): String {
        return "${super.toString()}(children=$components)"
    }
}

class RootPatternComponent : BasePatternComponent() {
    init {
        active = true
    }

    override fun isValidComponent(component: PatternComponent) = isRootComponent(component)

    fun match(text: String): Boolean {
        return super.match(text, 0, 0) != -1
    }
}

class OptionalPatternComponent : BasePatternComponent() {
    override fun isValidComponent(component: PatternComponent) = isRootComponent(component)

    override fun match(text: String, sibling: Int, soFar: Int): Int {
        var ret = 0
        components.forEachIndexed { i, component ->
            val thisRet = component.match(text, i, soFar + ret)
            if (thisRet == -1)
                return 0
            ret += thisRet
        }
        return ret
    }
}

class MutuallyExclusiveChoicesPatternComponent : BasePatternComponent() {
    override fun isValidComponent(component: PatternComponent) = component is MutuallyExclusiveChoicePatternComponent

    override fun match(text: String, sibling: Int, soFar: Int): Int {
        val i = 0
        for (component in components) {
            val thisRet = component.match(text, i, soFar)
            if (thisRet != -1)
                return thisRet
        }
        return -1
    }
}

class MutuallyExclusiveChoicePatternComponent : BasePatternComponent() {
    override fun isValidComponent(component: PatternComponent) = isRootComponent(component)
}

data class WordPatternComponent(var word: String = "") : PatternComponent() {

    override fun append(char: Char) {
        word += char
    }

    override fun match(text: String, sibling: Int, soFar: Int): Int {
        var found = ""
        var i = 0
        for (char in text.substring(soFar)) {
            if (!char.toString().isImportant()) {
                if (found.isEmpty()) {
                    i += 1
                    continue
                } else {
                    break
                }
            }
            found += char
            i += 1
        }
        return if (found.equalsCaseInsensitive(word)) i else -1
    }

    override fun empty() = word.isEmpty()
}

class EndPatternComponent : PatternComponent() {
    override fun match(text: String, sibling: Int, soFar: Int): Int {
        if (soFar == text.length) return 0
        val space = text.substring(soFar, text.lastIndex)
        return if (space.all { !it.toString().isImportant() }) 0 else -1
    }
}

private fun isRootComponent(component: PatternComponent) = when(component) {
    is OptionalPatternComponent -> true
    is MutuallyExclusiveChoicesPatternComponent -> true
    is WordPatternComponent -> true
    is EndPatternComponent -> true
    else -> false
}


fun String.isImportant(): Boolean {
    return IMPORTANT_CHARACTER_REGEX.matches(this)
}

fun String.equalsCaseInsensitive(other: String): Boolean {
    val literal = Regex.escape(this)
    return Regex(literal, RegexOption.IGNORE_CASE).matches(other)
}

private val IMPORTANT_CHARACTERS = listOf("'")
private val IMPORTANT_CHARACTER_REGEX = Regex("""(\p{LD}|""" + IMPORTANT_CHARACTERS.joinToString("|") { Regex.escape(it) } + """)+""")