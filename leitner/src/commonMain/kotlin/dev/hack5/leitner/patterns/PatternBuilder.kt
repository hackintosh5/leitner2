package dev.hack5.leitner.patterns

class Pattern(val pattern: String) {
    val component = RootPatternComponent()
    init {
        component.descend(WordPatternComponent())
        for (char in pattern) {
            when (char) {
                '[' -> {
                    component.ascend()
                    component.descend(OptionalPatternComponent())
                    component.descend(WordPatternComponent())
                }
                ']' -> {
                    component.ascend()
                    component.ascend()
                    component.descend(WordPatternComponent())
                }
                '{' -> {
                    component.ascend()
                    component.descend(MutuallyExclusiveChoicesPatternComponent())
                    component.descend(MutuallyExclusiveChoicePatternComponent())
                    component.descend(WordPatternComponent())
                }
                '/' -> {
                    component.ascend()
                    component.ascend()
                    component.descend(MutuallyExclusiveChoicePatternComponent())
                    component.descend(WordPatternComponent())
                }
                '}' -> {
                    component.ascend()
                    component.ascend()
                    component.ascend()
                    component.descend(WordPatternComponent())
                }
                else -> {
                    if (char.toString().isImportant()) {
                        component.append(char)
                    } else {
                        component.ascend()
                        component.descend(WordPatternComponent())
                    }
                }
            }
        }
        component.ascend()
        if (!component.active) {
            throw IllegalArgumentException("Unclosed ${component.activeComponent}")
        }
        component.descend(EndPatternComponent())
    }

    fun matches(text: String) = component.match(text)
}