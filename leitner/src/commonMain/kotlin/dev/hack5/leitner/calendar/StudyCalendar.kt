package dev.hack5.leitner.calendar

import dev.hack5.leitner.models.LeitnerLevels
import dev.hack5.leitner.models.LeitnerTerm
import dev.hack5.leitner.models.LeitnerTermSorter
import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.plus


class StudyCalendar(val terms: List<LeitnerTerm>, val frequencies: LeitnerLevels) {

    fun organize(firstDay: LocalDate): Map<LocalDate, List<LeitnerTerm>> {
        val termsByDay = mutableMapOf<LocalDate, MutableList<LeitnerTerm>>()
        for (term in terms.sortedWith(LeitnerTermSorter(frequencies))) {
            var bestDay: Pair<Int, LocalDate>? = null
            for (day in term.getNextStudyRange(frequencies)) {
                if (day < firstDay) continue
                val terms = termsByDay.getOrElse(day, ::emptyList)
                if (bestDay == null || terms.size < bestDay.first)
                    bestDay = terms.size to day
            }
            if (bestDay == null)
                bestDay = 0 to firstDay
            termsByDay.getOrPut(bestDay.second, ::mutableListOf).add(term)
        }
        return termsByDay
    }
}


fun LeitnerTerm.getStudyRange(frequencies: LeitnerLevels) = frequencies[level]

fun LeitnerTerm.getNextStudyRange(frequencies: LeitnerLevels): List<LocalDate> {
    val (start, end) = getStudyRange(frequencies)
    return (start..end).map { lastStudied.date + DatePeriod(days = it) }
}