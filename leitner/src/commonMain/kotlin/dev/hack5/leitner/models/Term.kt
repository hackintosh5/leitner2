package dev.hack5.leitner.models

import dev.hack5.leitner.calendar.getStudyRange
import kotlinx.datetime.*

interface Term

data class QuestionAnswerTerm(val question: String, val answer: String) : Term


data class LeitnerTerm(val term: Term, val lastStudied: LocalDateTime, val level: Int)

class LeitnerTermSorter(val frequencies: LeitnerLevels) : Comparator<LeitnerTerm> {
    override fun compare(a: LeitnerTerm, b: LeitnerTerm): Int {
        val (aStart, aEnd) = a.getStudyRange(frequencies)
        val aLength = aEnd - aStart
        val (bStart, bEnd) = b.getStudyRange(frequencies)
        val bLength = bEnd - bStart
        val lengthComparison = aLength.compareTo(bLength)
        if (lengthComparison != 0) return lengthComparison

        val startComparison = aStart.compareTo(bStart)
        if (startComparison != 0) return startComparison

        return 0
    }
}