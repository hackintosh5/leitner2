package dev.hack5.leitner.models

val FREQUENCIES = listOf(
    0 to 0,
    1 to 1,
    3 to 4,
    5 to 8,
    12 to 16,
    28 to 31,
)

typealias LeitnerLevels = List<LeitnerLevel>
typealias LeitnerLevel = Pair<Int, Int>