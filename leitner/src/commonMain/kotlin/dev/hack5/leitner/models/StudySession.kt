package dev.hack5.leitner.models

import kotlinx.datetime.LocalDate

data class StudySession(val terms: List<LeitnerTerm>, val date: LocalDate, val studied: MutableCollection<LeitnerTerm>)